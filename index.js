const discord = require('discord.js');
const client = new discord.Client();
const config = require('./config.json');

 client.on('ready', () => {
 	console.log('bot ready as '+client.user.username+'#'+client.user.discriminator);
 	client.user.setStatus('idle');
 	client.user.setActivity('bot is in '+client.guilds.length+' guilds');
 });

 client.on('message', message => {
 	if (message.author.bot) {
 		void(0)
 	}
 	else if (!message.author.bot) {
 		if (message.content.toLowerCase().includes('lol')) {
 			message.channel.send('lol').then(message => {
 				message.delete(10000);
 			});
 		}
 	}
 });

client.on('guildCreate', () => {
	client.user.setActivity('bot is in '+client.guilds.length+' guilds');
});

client.on('guildDelete', () => {
	client.user.setActivity('bot is in '+client.guilds.length+' guilds');
});

client.login(config.token);